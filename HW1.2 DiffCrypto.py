def xor(x1,x2): #XOR compares bits at same index
    output = []
    for i in range(0,len(x1)):
        if(x1[i] == x2[i]): #If bits are the same then output at that index 0
            output.append(0)
        else: #If bits are different then output at that index 1
            output.append(1)
    return output

def keyFunction(val, key, s0): 
    xorResult = xor(val,key)
    s0 = sBox(xorResult,s0) 
    return s0

def binaryConvert(val): #Simple interger to binary array converter
    arr = []
    val = bin(val)[2:]
    for i in range(0,len(val)):
        arr.append(int(val[i]))
    while(len(arr)<4): #Padding
        arr.insert(0,0)
    return arr

def binaryToInt(val): #Simple binary array to integer converter
    temp = ''
    for i in val:
        temp += str(i)
    return int(temp, 2)

def sBox(val, s0):
    col = 2 * val[1] + val[2]
    row = 2* val[0] + val[3]
    arr = []
    arr.append(s0[row][col] // 2)
    arr.append(s0[row][col] % 2)
    return arr

if __name__ == '__main__':
    s0 = [[1,0,3,2], [3,2,1,0], [0,2,1,3], [3,1,3,2]]
    key = [1,0,1,0]
    inputXOR = [0,0,1,0]

    for i in range(0,16): #Generate Distribution Table
        iBin = binaryConvert(i)
        iXOR = xor(iBin,inputXOR)

        sBox1 = sBox(iBin,s0)
        sBox2 = sBox(iXOR,s0)

        print('A: ' + str(i) + ' B: ' + str(binaryToInt(iXOR)) + ' C: ' + str(binaryToInt(xor(sBox1,sBox2))))

    #First pairs
    keyFnc1 = keyFunction(binaryConvert(1),key, s0)
    keyFnc2 = keyFunction(binaryConvert(3),key, s0)
    fXOR = xor(keyFnc1,keyFnc2)
    print(binaryToInt(fXOR))

    #Second pairs
    keyFnc1 = keyFunction(binaryConvert(4),key, s0)
    keyFnc2 = keyFunction(binaryConvert(6),key, s0)
    fXOR = xor(keyFnc1,keyFnc2)
    print(binaryToInt(fXOR))